/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.rendersystem.shader.glsl;

import com.jogamp.opengl.awt.*;

import ray.rage.rendersystem.*;
import ray.rage.rendersystem.shader.*;

/**
 * Concrete implementation of the {@link GpuShaderProgramFactory
 * shader-program-factory} interface.
 *
 * @author Raymond L. Rivera
 *
 */
public final class GlslProgramFactory implements GpuShaderProgramFactory {

    @Override
    public GpuShaderProgram createInstance(RenderSystem rs, GpuShaderProgram.Type type) {
        switch (type) {
            case RENDERING:
                return new GlslRenderingProgram((GLCanvas) rs.getCanvas());
            case SKYBOX:
                return new GlslSkyBoxProgram((GLCanvas) rs.getCanvas());
            default:
                throw new UnsupportedOperationException(type + " not implemented");
        }
    }

}
