/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.event.*;
import java.awt.geom.*;
import java.io.*;

import ray.rage.*;
import ray.rage.asset.texture.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.scene.Camera.Frustum.*;
import ray.rage.scene.controllers.*;
import ray.rage.util.*;
import ray.rml.*;

public class SkyBoxTest extends VariableFrameRateGame {

    private static final String NODE_NAME     = "CameraNode";
    private static final String SKYBOX_NAME   = "SkyBox";

    private Angle               angle         = new Degreef(5f);
    private boolean             skyBoxVisible = true;

    public SkyBoxTest() {
        super();
    }

    public static void main(String[] args) {
        Game test = new SkyBoxTest();
        try {
            test.startup();
            test.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            test.shutdown();
            test.exit();
        }
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera camera = sm.createCamera("MainCamera", Projection.PERSPECTIVE);
        camera.getFrustum().setNearClipDistance(0.1f);
        camera.setViewport(rw.getViewport(0));

        SceneNode cameraNode = sm.getRootSceneNode().createChildSceneNode(NODE_NAME);
        cameraNode.attachObject(camera);

        RotationController rc = new RotationController();
        rc.addNode(cameraNode);
        rc.setSpeed(-0.0035f);
        sm.addController(rc);
    }

    @Override
    protected void setupScene(Engine eng, SceneManager sm) throws IOException {
        Configuration conf = eng.getConfiguration();
        TextureManager textureMgr = getEngine().getTextureManager();

        textureMgr.setBaseDirectoryPath(conf.valueOf("assets.skyboxes.path"));
        Texture front = textureMgr.getAssetByPath("front.png");
        Texture back = textureMgr.getAssetByPath("back.png");
        Texture left = textureMgr.getAssetByPath("left.png");
        Texture right = textureMgr.getAssetByPath("right.png");
        Texture top = textureMgr.getAssetByPath("top.png");
        Texture bottom = textureMgr.getAssetByPath("bottom.png");
        textureMgr.setBaseDirectoryPath(conf.valueOf("assets.textures.path"));

        // cubemap textures must be flipped up-side-down to face inward; all
        // textures must have the same dimensions, so any image height will do
        AffineTransform xform = new AffineTransform();
        xform.translate(0, front.getImage().getHeight());
        xform.scale(1d, -1d);

        front.transform(xform);
        back.transform(xform);
        left.transform(xform);
        right.transform(xform);
        top.transform(xform);
        bottom.transform(xform);

        SkyBox sb = sm.createSkyBox(SKYBOX_NAME);
        sb.setTexture(front, SkyBox.Face.FRONT);
        sb.setTexture(back, SkyBox.Face.BACK);
        sb.setTexture(left, SkyBox.Face.LEFT);
        sb.setTexture(right, SkyBox.Face.RIGHT);
        sb.setTexture(top, SkyBox.Face.TOP);
        sb.setTexture(bottom, SkyBox.Face.BOTTOM);
        sm.setActiveSkyBox(sb);
    }

    @Override
    protected void update(Engine engine) {}

    @Override
    public void keyPressed(KeyEvent e) {
        Node cam = getEngine().getSceneManager().getRootSceneNode().getChild(NODE_NAME);
        switch (e.getKeyCode()) {
            case KeyEvent.VK_W:
            case KeyEvent.VK_UP:
                cam.pitch(angle.mult(-1));
                break;
            case KeyEvent.VK_A:
            case KeyEvent.VK_LEFT:
                cam.yaw(angle.mult(-1));
                break;
            case KeyEvent.VK_S:
            case KeyEvent.VK_DOWN:
                cam.pitch(angle);
                break;
            case KeyEvent.VK_D:
            case KeyEvent.VK_RIGHT:
                cam.yaw(angle);
                break;
            case KeyEvent.VK_R:
            case KeyEvent.VK_ENTER:
                // reset
                cam.lookAt(0, 0, -1);
                break;
            case KeyEvent.VK_SPACE:
                skyBoxVisible = !skyBoxVisible;
                getEngine().getSceneManager().getSkyBox(SKYBOX_NAME).setVisible(skyBoxVisible);
                break;
        }
        super.keyPressed(e);
    }

}
