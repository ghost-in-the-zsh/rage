/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.io.*;

import ray.rage.*;
import ray.rage.asset.*;
import ray.rage.asset.texture.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.scene.controllers.*;
import ray.rage.util.*;
import ray.rml.*;

/**
 * This test verifies that multiple {@link SkyBox skyboxes} can be used and
 * changed at runtime.
 * <p>
 * Note that this test has a work-around to get it to work. Currently, the
 * {@link TextureManager} can only load a {@link Texture} from disk once and
 * will return cached references on later attempts based on the specified name.
 * This is how the {@link AssetManager asset managers} are meant to work.
 * Therefore, I intentionally load {@link Textures} with different file
 * extension/names to trick the TM into loading several {@link SkyBox skyboxes}
 * simultaneously.
 * <p>
 * It's probably reasonable to expect users to give different names to all of
 * their {@link Texture textures} to work-around this limitation, similar to how
 * this test is doing it.
 *
 * @author Raymond L. Rivera
 *
 */
public class MultiSkyBoxTest extends VariableFrameRateGame {

    private final static float CAMERA_DISTANCE     = 8.0f;

    private SkyBox             skyBox1;
    private SkyBox             skyBox2;
    private Color              skyColor1           = Color.BLUE.brighter().brighter().brighter().brighter();
    private Color              skyColor2           = Color.YELLOW;
    private Light              skyLight;

    private boolean            autoRotationEnabled = true;

    private SceneNode          mainCameraNode;
    private SceneNode          asteroidNode;

    public MultiSkyBoxTest() {
        super();
    }

    public static void main(String[] args) {
        Game test = new MultiSkyBoxTest();
        try {
            test.startup();
            test.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            test.shutdown();
            test.exit();
        }
    }

    @Override
    protected void loadConfiguration(Configuration config) throws IOException {
        super.loadConfiguration(config);
        final String baseDirectory = "assets/skyboxes/oga/";
        config.setKeyValuePair("test.skybox1.path", baseDirectory + "galaxy/blue/");
        config.setKeyValuePair("test.skybox2.path", baseDirectory + "nebulae/");
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera mainCamera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        mainCamera.getFrustum().setNearClipDistance(0.1f);
        mainCamera.setViewport(rw.getViewport(0));

        mainCameraNode = sm.getRootSceneNode().createChildSceneNode(mainCamera.getName() + "Node");
        mainCameraNode.attachObject(mainCamera);
        mainCameraNode.moveBackward(CAMERA_DISTANCE);
    }

    @Override
    protected void setupScene(Engine eng, SceneManager sm) throws IOException {
        sm.getAmbientLight().setIntensity(new Color(.02f, .02f, .02f));
        setupAsteroid(sm);

        skyBox1 = createSkyBox(eng, sm, "SkyBox", 1, ".png");
        skyBox2 = createSkyBox(eng, sm, "SkyBox", 2, ".jpeg");

        sm.setActiveSkyBox(skyBox1);

        skyLight = sm.createLight("SkyBoxLight", Light.Type.DIRECTIONAL);
        skyLight.setDiffuse(skyColor1);
        skyLight.setSpecular(skyColor1);
        SceneNode lightNode = sm.getRootSceneNode().createChildSceneNode(skyLight.getName() + "Node");
        lightNode.attachObject(skyLight);
        lightNode.setLocalPosition(1, 1, 1);

        OrbitController oc = new OrbitController(asteroidNode);
        oc.addNode(mainCameraNode);
        oc.setDistanceFromTarget(CAMERA_DISTANCE);
        oc.setSpeed(.1f);
        oc.setAlwaysFacingTarget(true);

        RotationController rc = new RotationController(new Vector3f(1.1f, -1.15f, 1.2f));
        rc.addNode(asteroidNode);
        rc.setSpeed(.035f);

        sm.addController(oc);
        sm.addController(rc);
    }

    @Override
    protected void update(Engine engine) {
        for (Node.Controller nc : engine.getSceneManager().getControllers())
            nc.setEnabled(autoRotationEnabled);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_1:
                getEngine().getSceneManager().setActiveSkyBox(skyBox1);
                skyLight.setDiffuse(skyColor1);
                skyLight.setSpecular(skyColor1);
                break;
            case KeyEvent.VK_2:
                getEngine().getSceneManager().setActiveSkyBox(skyBox2);
                skyLight.setDiffuse(skyColor2);
                skyLight.setSpecular(skyColor2);
                break;
            case KeyEvent.VK_SPACE:
                autoRotationEnabled = !autoRotationEnabled;
                break;
        }
        super.keyPressed(e);
    }

    private SkyBox createSkyBox(Engine eng, SceneManager sm, String skyBoxName, int skyBoxId, String fileExtension)
            throws IOException {
        Configuration conf = eng.getConfiguration();
        TextureManager textureMgr = getEngine().getTextureManager();

        textureMgr.setBaseDirectoryPath(conf.valueOf("test.skybox" + skyBoxId + ".path"));
        Texture front = textureMgr.getAssetByPath("front" + fileExtension);
        Texture back = textureMgr.getAssetByPath("back" + fileExtension);
        Texture left = textureMgr.getAssetByPath("left" + fileExtension);
        Texture right = textureMgr.getAssetByPath("right" + fileExtension);
        Texture top = textureMgr.getAssetByPath("top" + fileExtension);
        Texture bottom = textureMgr.getAssetByPath("bottom" + fileExtension);

        // cubemap textures must be flipped up-side-down to face inward; all
        // textures must have the same dimensions, so any image height will do
        AffineTransform xform = new AffineTransform();
        xform.translate(0, front.getImage().getHeight());
        xform.scale(1d, -1d);

        front.transform(xform);
        back.transform(xform);
        left.transform(xform);
        right.transform(xform);
        top.transform(xform);
        bottom.transform(xform);

        SkyBox sb = sm.createSkyBox(skyBoxName + skyBoxId);
        sb.setTexture(front, SkyBox.Face.FRONT);
        sb.setTexture(back, SkyBox.Face.BACK);
        sb.setTexture(left, SkyBox.Face.LEFT);
        sb.setTexture(right, SkyBox.Face.RIGHT);
        sb.setTexture(top, SkyBox.Face.TOP);
        sb.setTexture(bottom, SkyBox.Face.BOTTOM);

        return sb;
    }

    private void setupAsteroid(SceneManager sm) throws IOException {
        Entity asteroid = sm.createEntity("Asteroid", "asteroid_3.obj");

        asteroidNode = sm.getRootSceneNode().createChildSceneNode(asteroid.getName() + "Node");
        asteroidNode.attachObject(asteroid);
    }

}
