/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import ray.rage.*;
import ray.rage.asset.material.*;
import ray.rage.asset.texture.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.rendersystem.states.*;
import ray.rage.scene.controllers.*;
import ray.rage.util.*;
import ray.rml.*;

public class MultiPointLightTest extends VariableFrameRateGame {

    private final static int   MAX_LIGHTS       = 3;
    private final static float DISTANCE_DELTA   = .075f;
    private Color              ambientIntensity = new Color(.02f, .02f, .02f);

    // @formatter:off
    private final Color[] colors = {
        Color.RED,
        Color.GREEN,
        Color.BLUE
    };
    // @formatter:on

    public MultiPointLightTest() {
        super();
    }

    public static void main(String[] args) {
        Game test = new MultiPointLightTest();
        try {
            test.startup();
            test.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            test.shutdown();
            test.exit();
        }
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera camera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        camera.getFrustum().setNearClipDistance(0.1f);
        camera.setViewport(rw.getViewport(0));

        SceneNode cameraNode = sm.getRootSceneNode().createChildSceneNode(camera.getName() + "Node");
        cameraNode.attachObject(camera);

        cameraNode.moveForward(5);
        cameraNode.lookAt(Point3f.origin());
    }

    @Override
    protected void setupScene(Engine engine, SceneManager sm) throws IOException {
        SceneNode rootNode = sm.getRootSceneNode();

        Entity target = sm.createEntity("TargetSphere", "sphere.obj");
        SceneNode targetNode = rootNode.createChildSceneNode(target.getName() + "Node");
        targetNode.attachObject(target);

        OrbitController ctrl = new OrbitController(targetNode);
        ctrl.setDistanceFromTarget(3f);
        ctrl.setVerticalDistance(.75f);
        sm.addController(ctrl);

        Configuration cfg = engine.getConfiguration();
        Texture tex = sm.getTextureManager().getAssetByPath(cfg.valueOf("assets.textures.default"));
        TextureState tstate = (TextureState) sm.getRenderSystem().createRenderState(RenderState.Type.TEXTURE);
        tstate.setTexture(tex);

        for (int i = 0; i < MAX_LIGHTS; ++i) {
            Entity e = sm.createEntity("LightSphere-" + i, "sphere.obj");
            e.setRenderState(tstate);

            Material m = sm.getMaterialManager().createManualAsset(e.getName() + "_Material");
            Color c = colors[i % colors.length];
            m.setAmbient(c);
            m.setDiffuse(c);
            m.setEmissive(c);
            m.setSpecular(c);
            e.setMaterial(m);

            Light l = sm.createLight("Light-" + i, Light.Type.POINT);
            l.setDiffuse(m.getEmissive());
            l.setSpecular(m.getSpecular());
            l.setRange(5f);

            SceneNode n = rootNode.createChildSceneNode(e.getName() + "Node");
            n.attachObject(e);
            n.attachObject(l);
            n.scale(.15f);

            ctrl.addNode(n);
        }
        sm.getAmbientLight().setIntensity(ambientIntensity);
    }

    @Override
    protected void update(Engine engine) {}

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        OrbitController ctrl = (OrbitController) getEngine().getSceneManager().getController(0);

        float d = ctrl.getDistanceFromTarget();
        if (e.getWheelRotation() < 0)
            d += DISTANCE_DELTA;
        else
            d -= DISTANCE_DELTA;

        if (d <= 1f)
            d = 1f;

        ctrl.setDistanceFromTarget(d);
    }

}
