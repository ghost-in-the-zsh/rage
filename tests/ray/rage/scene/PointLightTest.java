/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import ray.rage.*;
import ray.rage.asset.material.*;
import ray.rage.asset.texture.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.rendersystem.states.*;
import ray.rage.scene.controllers.*;
import ray.rml.*;

public class PointLightTest extends VariableFrameRateGame {

    private final static String LIGHT_NAME       = "LightSource";
    private final static float  DISTANCE_DELTA   = .075f;
    private Color               ambientIntensity = new Color(.02f, .02f, .02f);

    public PointLightTest() {
        super();
    }

    public static void main(String[] args) {
        Game test = new PointLightTest();
        try {
            test.startup();
            test.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            test.shutdown();
            test.exit();
        }
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera camera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        camera.getFrustum().setNearClipDistance(0.1f);
        camera.setViewport(rw.getViewport(0));

        SceneNode cameraNode = sm.getRootSceneNode().createChildSceneNode(camera.getName() + "Node");
        cameraNode.attachObject(camera);

        // place camera at location where problems have been spotted before
        cameraNode.moveForward(8);
        cameraNode.lookAt(Point3f.origin());
    }

    @Override
    protected void setupScene(Engine engine, SceneManager sm) throws IOException {
        SceneNode rootNode = sm.getRootSceneNode();

        Entity target = sm.createEntity("Target", "sphere.obj");
        SceneNode targetNode = rootNode.createChildSceneNode(target.getName() + "Node");
        targetNode.attachObject(target);

        Entity source = sm.createEntity("Source", "sphere.obj");
        Material mat = sm.getMaterialManager().getAssetByPath("default.mtl");
        mat.setEmissive(Color.WHITE);

        Texture tex = sm.getTextureManager().getAssetByPath(mat.getTextureFilename());
        TextureState tstate = (TextureState) sm.getRenderSystem().createRenderState(RenderState.Type.TEXTURE);
        tstate.setTexture(tex);
        source.setRenderState(tstate);
        source.setMaterial(mat);

        Light light = sm.createLight(LIGHT_NAME, Light.Type.POINT);
        light.setDiffuse(mat.getEmissive());
        light.setRange(5f);
        SceneNode sourceNode = rootNode.createChildSceneNode(source.getName() + "Node");
        sourceNode.attachObject(source);
        sourceNode.attachObject(light);
        sourceNode.scale(.15f);

        sm.getAmbientLight().setIntensity(ambientIntensity);

        OrbitController ctrl = new OrbitController(targetNode);
        ctrl.setDistanceFromTarget(5f);
        ctrl.setVerticalDistance(.75f);
        ctrl.addNode(sourceNode);
        sm.addController(ctrl);
    }

    @Override
    protected void update(Engine engine) {}

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_R:
                updateLightColor(Color.RED);
                break;
            case KeyEvent.VK_G:
                updateLightColor(Color.GREEN);
                break;
            case KeyEvent.VK_B:
                updateLightColor(Color.BLUE);
                break;
            case KeyEvent.VK_W:
                updateLightColor(Color.WHITE);
                break;
        }
        super.keyPressed(e);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        OrbitController ctrl = (OrbitController) getEngine().getSceneManager().getController(0);

        float d = ctrl.getDistanceFromTarget();
        if (e.getWheelRotation() < 0)
            d += DISTANCE_DELTA;
        else
            d -= DISTANCE_DELTA;

        try {
            ctrl.setDistanceFromTarget(d);
        } catch (Exception ex) {
            // pass
        }
    }

    private void updateLightColor(Color c) {
        Light l = getEngine().getSceneManager().getLight(LIGHT_NAME);
        l.setDiffuse(c);
        l.setSpecular(c);
        getEngine().getSceneManager().getEntity("Source").getSubEntity(0).getMaterial().setEmissive(c);
    }

}
