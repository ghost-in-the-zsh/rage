/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene.controllers;

import java.awt.*;
import java.io.*;

import ray.rage.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.scene.*;
import ray.rml.*;

public final class WaypointControllerTest extends VariableFrameRateGame {

    private WaypointController waypointController = new WaypointController();

    public static void main(String[] args) {
        Game test = new WaypointControllerTest();
        try {
            test.startup();
            test.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            test.shutdown();
            test.exit();
        }
    }

    @Override
    protected void update(Engine engine) {}

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera camera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        camera.getFrustum().setNearClipDistance(0.1f);
        camera.setViewport(rw.getViewport(0));

        SceneNode cameraNode = sm.getRootSceneNode().createChildSceneNode(camera.getName() + "Node");
        cameraNode.attachObject(camera);
        cameraNode.moveBackward(10);
    }

    @Override
    protected void setupScene(Engine engine, SceneManager sm) throws IOException {
        sm.getAmbientLight().setIntensity(Color.LIGHT_GRAY);

        Entity cube = sm.createEntity("Cube", "cube.obj");
        SceneNode cubeNode = sm.getRootSceneNode().createChildSceneNode(cube.getName() + "Node");
        cubeNode.attachObject(cube);

        waypointController.addNode(cubeNode);
        waypointController.setIntervalTimeMillis(2000);
        setupWaypoints(waypointController);
        sm.addController(waypointController);
    }

    private void setupWaypoints(WaypointController ctrl) {
        final float x = 5;
        final float y = 3;
        final float z = 0;
        // @formatter:off
        final float[][] waypoints = new float[][] {
            {-x,  y, z},
            { x,  y, z},
            { x, -y, z},
            {-x, -y, z},
        };
        // @formatter:on

        for (float[] wp : waypoints)
            ctrl.addWaypoint(new Point3f(wp));
    }

}
