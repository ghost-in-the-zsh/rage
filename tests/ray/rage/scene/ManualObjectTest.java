/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.nio.*;

import ray.rage.*;
import ray.rage.asset.material.*;
import ray.rage.asset.texture.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.rendersystem.Renderable.*;
import ray.rage.rendersystem.shader.*;
import ray.rage.rendersystem.states.*;
import ray.rage.rendersystem.states.RenderState.*;
import ray.rage.scene.Node.*;
import ray.rage.scene.controllers.*;
import ray.rage.util.*;
import ray.rml.*;

public class ManualObjectTest extends VariableFrameRateGame {

    private final Angle rotationAngle = new Degreef(5.5f);
    private boolean     autoRotate    = true;

    public ManualObjectTest() {
        super();
    }

    public static void main(String[] args) {
        Game game = new ManualObjectTest();
        try {
            game.startup();
            game.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            game.shutdown();
            game.exit();
        }
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera camera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        camera.getFrustum().setNearClipDistance(0.1f);
        camera.setViewport(rw.getViewport(0));

        SceneNode cameraNode = sm.getRootSceneNode().createChildSceneNode("CameraNode");
        cameraNode.attachObject(camera);
        cameraNode.setLocalPosition(0, 0, 5);
    }

    @Override
    protected void setupScene(Engine engine, SceneManager sm) throws IOException {
        sm.getAmbientLight().setIntensity(new Color(.025f, .025f, .025f));

        ManualObject cube = createManualObject(engine, sm);
        SceneNode cubeNode = sm.getRootSceneNode().createChildSceneNode("CubeNode");
        cubeNode.attachObject(cube);

        Light light = sm.createLight(cube.getName() + "Light", Light.Type.DIRECTIONAL);
        light.setDiffuse(Color.WHITE);
        SceneNode lightNode = sm.getRootSceneNode().createChildSceneNode(light.getName() + "Node");
        lightNode.attachObject(light);
        lightNode.setLocalPosition(1, .5f, 2);

        RotationController rc = new RotationController(new Vector3f(1, 1, 1));
        rc.addNode(cubeNode);
        sm.addController(rc);
    }

    private ManualObject createManualObject(Engine eng, SceneManager sm) throws IOException {
        ManualObject cube = sm.createManualObject("Cube");

        // while the ManualObject could've been made of a single
        // ManualObjectSection with single (and smaller)
        // vertex/texcoords/normals buffers, doing it this way allows for
        // a better test case
        ManualObjectSection front = cube.createManualSection("Front");
        ManualObjectSection back = cube.createManualSection("Back");
        ManualObjectSection left = cube.createManualSection("Left");
        ManualObjectSection right = cube.createManualSection("Right");
        ManualObjectSection top = cube.createManualSection("Top");
        ManualObjectSection bottom = cube.createManualSection("Bottom");

        // the program is set after the sections are created, so that the same
        // program is used for all of them
        cube.setGpuShaderProgram(sm.getRenderSystem().getGpuShaderProgram(GpuShaderProgram.Type.RENDERING));

        // assign specific vertices to each face of the cube
        front.setVertexBuffer(getVerticesFront());
        back.setVertexBuffer(getVerticesBack());
        left.setVertexBuffer(getVerticesLeft());
        right.setVertexBuffer(getVerticesRight());
        top.setVertexBuffer(getVerticesTop());
        bottom.setVertexBuffer(getVerticesBottom());

        // set normal vectors for each face, pointing 'out' of the face
        front.setNormalsBuffer(BufferUtil.directFloatBuffer(new float[] { 0, 0, 1 }));
        back.setNormalsBuffer(BufferUtil.directFloatBuffer(new float[] { 0, 0, -1 }));
        left.setNormalsBuffer(BufferUtil.directFloatBuffer(new float[] { -1, 0, 0 }));
        right.setNormalsBuffer(BufferUtil.directFloatBuffer(new float[] { 1, 0, 0 }));
        top.setNormalsBuffer(BufferUtil.directFloatBuffer(new float[] { 0, 1, 0 }));
        bottom.setNormalsBuffer(BufferUtil.directFloatBuffer(new float[] { 0, -1, 0 }));

        // use the same texture coords and indices for all faces
        cube.setTextureCoordBuffer(getTexCoords());
        cube.setIndexBuffer(getIndices());

        Material mat = eng.getMaterialManager().createManualAsset("CubeMaterial");
        Texture tex = eng.getTextureManager().getAssetByPath("hexagons.jpeg");
        TextureState texState = (TextureState) sm.getRenderSystem().createRenderState(Type.TEXTURE);
        texState.setTexture(tex);

        FrontFaceState faceState = (FrontFaceState) sm.getRenderSystem().createRenderState(Type.FRONT_FACE);
        ZBufferState zstate = (ZBufferState) sm.getRenderSystem().createRenderState(Type.ZBUFFER);

        // use the same material and render state on all faces
        cube.setDataSource(DataSource.INDEX_BUFFER);
        cube.setRenderState(texState);
        cube.setRenderState(faceState);
        cube.setRenderState(zstate);
        cube.setMaterial(mat);

        return cube;
    }

    @Override
    protected void update(Engine engine) {
        for (Controller ctrl : engine.getSceneManager().getControllers())
            ctrl.setEnabled(autoRotate);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        SceneManager sm = getEngine().getSceneManager();

        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                sm.getSceneNode("CubeNode").yaw(rotationAngle.mult(-1));
                break;
            case KeyEvent.VK_RIGHT:
                sm.getSceneNode("CubeNode").yaw(rotationAngle);
                break;
            case KeyEvent.VK_UP:
                sm.getSceneNode("CubeNode").pitch(rotationAngle.mult(-1));
                break;
            case KeyEvent.VK_DOWN:
                sm.getSceneNode("CubeNode").pitch(rotationAngle);
                break;
            case KeyEvent.VK_SPACE:
                autoRotate = !autoRotate;
                break;
        }
        super.keyPressed(e);
    }

    private static FloatBuffer getVerticesFront() {
        // @formatter:off
        float[] vertices = new float[] {
            -1f,  1f, 1f,
            -1f, -1f, 1f,
             1f, -1f, 1f,
             1f,  1f, 1f
        };
        // @formatter:on
        return BufferUtil.directFloatBuffer(vertices);
    }

    private static FloatBuffer getVerticesBack() {
        // @formatter:off
        float[] vertices = new float[] {
             1f,  1f, -1f,
             1f, -1f, -1f,
            -1f, -1f, -1f,
            -1f,  1f, -1f
        };
        // @formatter:on
        return BufferUtil.directFloatBuffer(vertices);
    }

    private static FloatBuffer getVerticesLeft() {
        // @formatter:off
        float[] vertices = new float[] {
            -1f,  1f, -1f,
            -1f, -1f, -1f,
            -1f, -1f,  1f,
            -1f,  1f,  1f
        };
        // @formatter:on
        return BufferUtil.directFloatBuffer(vertices);
    }

    private static FloatBuffer getVerticesRight() {
        // @formatter:off
        float[] vertices = new float[] {
            1f,  1f,  1f,
            1f, -1f,  1f,
            1f, -1f, -1f,
            1f,  1f, -1f
        };
        // @formatter:on
        return BufferUtil.directFloatBuffer(vertices);
    }

    private static FloatBuffer getVerticesTop() {
        // @formatter:off
        float[] vertices = new float[] {
            -1f,  1f, -1f,
            -1f,  1f,  1f,
             1f,  1f,  1f,
             1f,  1f, -1f
        };
        // @formatter:on
        return BufferUtil.directFloatBuffer(vertices);
    }

    private static FloatBuffer getVerticesBottom() {
        // @formatter:off
        float[] vertices = new float[] {
            -1f, -1f,  1f,
            -1f, -1f, -1f,
             1f, -1f, -1f,
             1f, -1f,  1f
        };
        // @formatter:on
        return BufferUtil.directFloatBuffer(vertices);
    }

    private static FloatBuffer getTexCoords() {
        // @formatter:off
        float[] texcoords = new float[] {
            0, 1,
            0, 0,
            1, 0,
            1, 1
        };
        // @formatter:on
        return BufferUtil.directFloatBuffer(texcoords);
    }

    private static IntBuffer getIndices() {
        // @formatter:off
        int[] indices = new int[] {
            0, 1, 2,
            0, 2, 3
        };
        // @formatter:on
        return BufferUtil.directIntBuffer(indices);
    }

}
